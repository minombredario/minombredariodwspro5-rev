<?php

    class Config {

        static public $titulo = "Gestión de Asignaturas";

        static public $autor = "Darío Navarro";

        static public $fecha = "19/01/17";

        static public $empresa = "CEEDCV";

        static public $curso = "2016-17";

        static public $tema = "Tema 5. Ficheros";

        static public $modelo = "mysql";

        static public $bdhostname = "localhost";

        static public $bdnombre = "ceedcv";

        static public $bdusuario = "alumno";

        static public $bdclave = "alumno";
        
        static public $Documentacion = array("Tema5.POO");
        

    }
?>

