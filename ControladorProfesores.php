<?php
  
    include ("Profesor.php");
    require ("funciones.php");
    
    
    function guardar() {
        $modelo = new ModeloFichero();
        $id = recoge('id');
        $nombre = recoge('nombre');

        if ($nombre != "") {
            $profesor = new Profesor($id, $nombre);
            $modelo->createProfesor($profesor);
            header("Location:VistaProfesores.php") ;
        }
    }

    function borrar(){
        
        if(isset($_GET['del'])){
        $modelo = new ModeloFichero();
        $id = recoge('del');
        
        if ($id != ""){
            $profesor = new Profesor($id, "");
            $modelo->deleteProfesor($profesor);
            header("Location:VistaProfesores.php") ;
        }
    }
    }    
        

?>
